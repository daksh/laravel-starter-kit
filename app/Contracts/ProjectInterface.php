<?php

namespace App\Contracts;

interface ProjectInterface
{
    /**
     * Return number of total tasks
     * @return integer
     */
    public function noOfTasks();

    /**
     * Return number of completed tasks
     * @return integer
     */
    public function noOfCompletedTasks();

    /**
     * Return number of incomplete tasks
     * @return integer
     */
    public function noOfPendingTasks();

    /**
     * Return formatted total time worked on this project
     * @return string Formatted in unpaid hours and minutes
     */
    public function totalTime();

    /**
     * Return formatted unpaid time worked on this project
     * @return string Formatted in unpaid hours and minutes
     */
    public function totalUnpaidTime();
}
