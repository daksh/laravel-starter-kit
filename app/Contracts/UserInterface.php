<?php

namespace App\Contracts;

interface UserInterface
{
    /**
     * Return the active workspace for User
     * @return App\Workspace Obect of active workspace
     */
    public function workspace();

    /**
     * Update the active workspace of the user
     * @param  integer $workspaceId ID of the workspace to be actived
     * @return boolean            Boolean of status
     */
    public function updateActiveWorkspace($workspaceId);

    /**
     * Return number of pending/incomplete tasks
     * of current active workspace
     * @return integer
     */
    public function noOfPendingTasks();
}
