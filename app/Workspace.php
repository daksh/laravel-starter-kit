<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workspace extends Model
{
    protected $guarded = [];

    public function users()
    {
        return $this->belongsToMany('App\User', 'users_workspaces')->withPivot(['active']);
    }

    public function projects()
    {
        return $this->hasMany('App\Project');
    }

    public function entries()
    {
        return $this->hasMany('App\TimeEntry');
    }
}
