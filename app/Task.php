<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\SelectWorkspaceTrait;

class Task extends Model
{
    use SelectWorkspaceTrait;

    protected $guarded = [];

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function assignee()
    {
        return $this->belongsTo('App\User');
    }

    public function entries()
    {
        return $this->hasMany('App\TimeEntry');
    }

    public function scopeCompleted($query)
    {
        $query->where('completed', true);
    }

    public function scopeIncomplete($query)
    {
        $query->where('completed', false);
    }
}
