<?php

namespace App\Jobs;

use App\Asana;
use App\Contracts\ProjectInterface;
use App\Contracts\UserInterface;
use App\Jobs\Job;
use App\Task;
use App\User;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Cache;

class SyncTasks extends Job implements SelfHandling, ShouldQueue
{
    protected $asana;
    protected $project;

    use InteractsWithQueue, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Asana $asana, ProjectInterface $project)
    {
        //
        $this->asana = $asana;
        $this->project = $project;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $tasks = $this->asana->client->tasks->findAll([
            'project' => $this->project->asana_id
        ], [
            'fields' => ['id', 'name', 'assignee', 'completed', 'notes', 'tags']
        ]);

        $updatedTasks = [];

        foreach ($tasks as $t) {
            $user = null;
            if ($t->assignee != null) {
                $user = User::where('asana_id', $t->assignee->id)->first();

                if ($user) {
                    $user = $user->id;
                }
            }

            $task = Task::firstOrCreate([
                'asana_id' => $t->id,
                'project_id' => $this->project->id,
                'workspace_id' => $this->project->workspace_id
            ]);

            $task->title = $t->name;
            $task->description = $t->notes;
            $task->completed = $t->completed;
            $task->user_id = $user;

            // Detect billable status from tag
            $task->billable = true;
            if (count($t->tags)) {
                foreach ($t->tags as $tag) {
                    if (Cache::get('non_billable_tag'.$this->project->workspace_id, null) == $tag->id) {
                        $task->billable = false;
                    }
                }
            }

            $task->save();

            $updatedTasks[] = $task->id;
        }

        // Delete rest of the tasks
        Task::where('workspace_id', $this->project->workspace_id)
            ->where('project_id', $this->project->id)
            ->whereNotIn('id', $updatedTasks)->delete();
    }
}
