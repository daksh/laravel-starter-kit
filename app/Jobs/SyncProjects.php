<?php

namespace App\Jobs;

use App\Asana;
use App\Contracts\UserInterface;
use App\Jobs\Job;
use App\Jobs\SyncTasks;
use App\Project;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Sentinel;

class SyncProjects extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels, DispatchesJobs;

    protected $asana;
    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Asana $asana, UserInterface $user)
    {
        //
        $this->asana = $asana;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $workspace = $this->user->workspace();
        $projects = $this->asana->client->projects->findAll([
            'workspace' => $workspace->asana_id
        ], [
            'fields' => ['id', 'name', 'archived']
        ]);

        foreach ($projects as $p) {
            $project = Project::firstOrCreate([
                'asana_id' => $p->id,
                'workspace_id' => $workspace->id,
            ]);

            $project->name = $p->name;
            $project->archived = $p->archived;

            $project->save();

            $this->dispatch(new SyncTasks($this->asana, $project, $this->user));
        }
    }
}
