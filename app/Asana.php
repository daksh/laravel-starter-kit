<?php

namespace App;

use Asana\Client;
use Session;

class Asana
{
    public $client;

    public function __construct()
    {
        $opts = [
            'client_id' => config('services.asana.client_id'),
            'client_secret' => config('services.asana.secret'),
            'redirect_uri' => url('/asana/authorized'),
            'token' => (Session::has('asana_token') ? Session::get('asana_token') : null)
        ];

        $this->client = Client::oauth($opts);

//        $this->client->options['iterator_type'] = false;
    }

    public function getAuthUrl()
    {
        return $this->client->dispatcher->authorizationUrl();
    }

    public function setToken($token)
    {
        $token = $this->client->dispatcher->fetchToken($token);
        Session::put('asana_token', $token);
    }

    public function isAuthorized()
    {
        return $this->client->dispatcher->authorized;
    }

    public function user()
    {
        return $this->client->users->me();
    }
}
