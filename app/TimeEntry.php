<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeEntry extends Model
{
    protected $guarded = [];
    protected $dates = ['start', 'end'];

    public function task()
    {
        return $this->belongsTo('App\Task');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function workspace()
    {
        return $this->belongsTo('App\Workspace');
    }
}
