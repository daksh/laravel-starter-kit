<?php namespace App;

use App\Contracts\UserInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Kit\Models\User as KitUser;
use Illuminate\Contracts\Auth\Authenticatable as BasicAuth;
use Cache;
use Illuminate\Auth\Authenticatable;

class User extends KitUser implements UserInterface, BasicAuth
{
    use Authenticatable;

    public function entries()
    {
        return $this->hasMany('App\TimeEntry');
    }

    public function workspaces()
    {
        return $this->belongsToMany('App\Workspace', 'users_workspaces')->withPivot(['active']);
    }

    public function projects()
    {
        $myId = $this->id;

        return Project::select('projects.*')->distinct()->join('tasks', function ($join) use (&$myId) {
            $join->on('projects.id', '=', 'tasks.project_id')->where('user_id', '=', $myId);
        });
    }

    public function workspace()
    {
        return Cache::get('workspace'.$this->id, function () {
            \Log::debug('From db');
            $w = $this->workspaces()->wherePivot('active', true)->firstOrFail();

            Cache::forever('workspace'.$this->id, $w);

            return $w;
        });
    }

    public function updateActiveWorkspace($workspaceId)
    {
        try {
            $w = $this->workspace();
            $w->pivot->active = false;
            $w->pivot->save();

            Cache::forget('workspace'.$this->id);

        } catch (ModelNotFoundException $e) {
        }

        // Find the Non-Billable tag
        $workspace = Workspace::findOrFail($workspaceId);

        if (! Cache::has('non_billable_tag'.$workspace->id)) {
            $asana = new Asana;
            $tags = $asana->client->tags->findAll(['workspace' => $workspace->asana_id]);

            foreach ($tags as $tag) {
                if ($tag->name == 'non-billable') {
                    // Okay, we found it..
                    Cache::forever('non_billable_tag'.$workspaceId, $tag->id);
                    break;
                }
            }
        }


        return $this->workspaces()
            ->updateExistingPivot($workspaceId, ['active' => true]);
    }

    public function myTasks()
    {
        return $this->hasMany('App\Task');
    }

    public function noOfPendingTasks()
    {
        return $this->myTasks()->where('completed', false)->count('id');
    }
}
