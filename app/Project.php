<?php

namespace App;

use App\Contracts\ProjectInterface;
use App\Scopes\SelectWorkspaceTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Project extends Model implements ProjectInterface
{
    use SelectWorkspaceTrait;

    protected $guarded = [];

    public function scopeActive($query)
    {
        $query->where('archived', false);
    }

    public function workspace()
    {
        return $this->belongsTo('App\Workspace');
    }

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }

    public function noOfTasks()
    {
        return $this->tasks()->count('id');
    }

    public function noOfCompletedTasks()
    {
        return $this->tasks()->completed()->count('id');
    }

    public function noOfPendingTasks()
    {
        return $this->tasks()->incomplete()->count('id');
    }

    public function totalTime()
    {
        $tasks = $this->tasks()->with('entries')->get();

        $seconds = null;

        foreach ($tasks as $task) {
            foreach ($task->entries as $entry) {
                $seconds += $entry->end->diffInSeconds($entry->start);
            }
        }

        if ($seconds == 0) {
            return 'nothing';
        }

        $d1 = Carbon::now();
        $d2 = Carbon::now()->addSeconds($seconds);

        $hours = $d2->diffInHours($d1);
        $mins = $d2->diffInMinutes($d1) - ($hours*60);
        $secs = $d2->diffInSeconds($d1) - (($hours*60*60) + ($mins*60));

        return $hours.'h '.$mins.'m '.$secs.'s';
    }

    public function totalUnpaidTime()
    {
        $tasks = $this->tasks()->with(['entries' => function ($q) {
            $q->where('paid', false);
        }])->get();

        $seconds = null;

        foreach ($tasks as $task) {
            foreach ($task->entries as $entry) {
                $seconds += $entry->end->diffInSeconds($entry->start);
            }
        }

        if ($seconds == 0) {
            return 'nothing';
        }

        $d1 = Carbon::now();
        $d2 = Carbon::now()->addSeconds($seconds);

        $hours = $d2->diffInHours($d1);
        $mins = $d2->diffInMinutes($d1) - ($hours*60);
        $secs = $d2->diffInSeconds($d1) - (($hours*60*60) + ($mins*60));

        return $hours.'h '.$mins.'m '.$secs.'s';
    }
}
