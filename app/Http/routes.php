<?php

use App\Asana;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::controller('asana', 'Asana\AuthController');
Route::controller('workspaces', 'Asana\WorkspaceController');
Route::controller('projects', 'Asana\ProjectsController');
Route::controller('tasks', 'Asana\TasksController');
Route::controller('timesheet', 'TimesheetController');

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['namespace' => 'App\Http\Controllers\Api', 'middleware' => 'auth.basic'], function ($api) {
        $api->resource('workspaces', 'WorkspacesController');
        $api->resource('projects', 'ProjectsController');
        $api->resource('entries', 'TimeEntriesController');
    });
});
