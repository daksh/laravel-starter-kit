<?php

namespace App\Http\Controllers\Asana;

use App\Asana;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Jobs\SyncTasks;
use App\Project;
use App\Task;
use Illuminate\Http\Request;
use Sentinel;

class TasksController extends Controller
{
    protected $asana;

    public function __construct(Asana $asana)
    {
        $this->asana = $asana;
    }

    public function getIndex($projectId)
    {
        $tasks = Task::where('project_id', $projectId)->all();

        return view('tasks.index', compact('tasks'));
    }

    public function getSync($projectId)
    {
        $project = Project::findOrFail($projectId);

        $this->dispatch(new SyncTasks($this->asana, $project));

        return redirect('/projects');
    }
}
