<?php

namespace App\Http\Controllers\Asana;

use App\Asana;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Workspace;
use Illuminate\Http\Request;
use Sentinel;

class WorkspaceController extends Controller
{
    protected $asana;
    protected $user;

    public function __construct(Asana $asana)
    {
        $this->asana = $asana;
        $this->user = Sentinel::getUser();
    }

    public function getChange($workspace)
    {
        $this->user->updateActiveWorkspace($workspace);

        return redirect('/');
    }
}
