<?php

namespace App\Http\Controllers\Asana;

use App\Asana;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use App\Workspace;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Input;
use Redirect;
use Sentinel;

class AuthController extends Controller
{
    protected $asana;

    public function __construct(Asana $asana)
    {
        $this->asana = $asana;
    }

    public function getLogin()
    {
        $url = $this->asana->getAuthUrl();

        return Redirect::away($url);
    }

    public function getAuthorized()
    {
        $this->asana->setToken(Input::get('code'));

        $u = $this->asana->user();

        // Register User if not
        $user = User::where('asana_id', $u->id)->first();

        if (! $user) {
            $name = explode(' ', $u->name);

            $user = Sentinel::registerAndActivate([
                'first_name' => $name[0],
                'last_name' => $name[1],
                'email' => $u->email,
                'password' => 'password',
            ]);

            $user->asana_id = $u->id;

            $user->save();
        }

        // Sync Workspaces
        $workspaces = [];
        foreach ($u->workspaces as $w) {
            $workspace = Workspace::where('asana_id', $w->id)->first();

            if (! $workspace) {
                $workspace = new Workspace;

                $workspace->name = $w->name;
                $workspace->asana_id = $workspace->id;

                $workspace->save();
            }

            $workspaces[] = $workspace->id;
        }

        $user->workspaces()->sync($workspaces);

        // Login User
        Sentinel::login($user);

        // Set Active Workspace if none selected
        try {
            $user->workspace();
        } catch (ModelNotFoundException $e) {
            $user->updateActiveWorkspace($user->workspaces()->first()->id);
        }

        return redirect('/');
    }
}
