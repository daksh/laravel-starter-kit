<?php

namespace App\Http\Controllers\Asana;

use App\Asana;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Jobs\SyncProjects;
use Illuminate\Http\Request;
use Sentinel;

class ProjectsController extends Controller
{
    protected $asana;

    public function __construct(Asana $asana)
    {
        $this->asana = $asana;
    }

    public function getIndex()
    {
        $projects = Sentinel::getUser()->projects()->get();

        return view('projects.index', compact('projects'));
    }

    public function getSync()
    {
        $this->dispatch(new SyncProjects($this->asana, Sentinel::getUser()));

        return redirect('/');
    }
}
