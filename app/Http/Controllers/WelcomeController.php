<?php namespace App\Http\Controllers;

use App\Asana;
use Sentinel;

class WelcomeController extends Controller
{
    public function __construct()
    {
        if (! Sentinel::check()) {
            return redirect('asana/login');
        }
    }

    /*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index()
    {
        $data['total_projects'] = Sentinel::getUser()->projects()->active()->count('projects.id');
        $data['pending_tasks'] = Sentinel::getUser()->noOfPendingTasks();

        return view('welcome', $data);
    }
}
