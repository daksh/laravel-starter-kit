<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\TimeEntry;
use Carbon\Carbon;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Validator;

class TimeEntriesController extends BaseController
{
    protected $auth;

    public function __construct(Request $request, TimeEntry $entries, Guard $auth)
    {
        parent::__construct($request);

        $this->model = $entries;
        $this->auth = $auth;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'task_id' => 'required|exists:tasks,id',
            'duration' => 'required|numeric',
        ]);

        if ($validator->passes()) {
            $startDate = Carbon::parse($request->get('date'));
            $entry = $this->model->newInstance();

            $entry->task_id = $request->task_id;
            $entry->start = $startDate;
            $entry->end = $startDate->addSeconds($request->get('duration'));
            $entry->user_id = $this->auth->user()->id;
            $entry->workspace_id = $this->auth->user()->workspace()->id;


            $entry->save();

            return $this->response->created();
        }

        throw new StoreResourceFailedException('Can not make time entry.', $validator->errors());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
