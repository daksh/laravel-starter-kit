<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;

class BaseController extends Controller
{
    use Helpers;

    protected $model = null;
    protected $request = null;
    protected $where = [];

    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->prepareModel();
    }

    protected function setLimit()
    {
        if ($this->request->has('limit')) {
            $this->model = $this->model->take($this->request->get('limit'));
        }
    }

    protected function prepareWheres()
    {
        if (count($this->where) > 0) {
            foreach ($this->where as $where) {
                if ($this->request->has($where)) {
                    $this->model = $this->model->where($where, $this->request->get($where));
                }
            }
        }
    }

    protected function prepareModel()
    {
        $this->setLimit();
        $this->prepareWheres();
    }
}
