<?php

namespace App\Http\Controllers\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Http\Controllers\Api\Transformers\ProjectTransformer;

class WorkspaceTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $availableIncludes = [
        'projects'
    ];

    public function transform($workspace)
    {
        return [
            'id' => (int) $workspace->id,
            'asana_id' => $workspace->asana_id,
            'name' => $workspace->name,
        ];
    }

    public function includeProjects($workspace)
    {
        return $this->collection($workspace->projects, new ProjectTransformer);
    }
}
