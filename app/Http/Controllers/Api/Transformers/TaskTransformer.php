<?php

namespace App\Http\Controllers\Api\Transformers;

use League\Fractal\TransformerAbstract;

class TaskTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $availableIncludes = [
        'project', 'workspace', 'assignee'
    ];

    public function transform($task)
    {
        return [
            'id' => (int) $task->id,
            'title' => $task->title,
            'description' => $task->description,
            'completed' => (bool) $task->completed,
            'assignee_id' => (int)$task->user_id,
            'project_id' => (int)$task->project_id,
            'workspace_id' => (int)$task->workspace_id,
        ];
    }

    public function includeProject($task)
    {
        $project = $task->project;

        return $this->item($project, new TaskTransformer);
    }

    public function includeWorkspace($task)
    {
        return $this->item($task->workspace, new WorkspaceTransformer);
    }
}
