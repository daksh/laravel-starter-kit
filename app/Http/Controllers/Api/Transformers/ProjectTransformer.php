<?php

namespace App\Http\Controllers\Api\Transformers;

use Illuminate\Auth\Guard;
use League\Fractal\TransformerAbstract;

class ProjectTransformer extends TransformerAbstract
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $availableIncludes = [
        'workspace', 'tasks'
    ];

    public function transform($project)
    {
        return [
            'id' => (int) $project->id,
            'asana_id' => $project->asana_id,
            'name' => $project->name,
            'archived' => (bool) $project->archived,
            'workspace_id' => $project->workspace_id,
        ];
    }

    public function includeWorkspace($project)
    {
        return $this->item($project->workspace, new WorkspaceTransformer);
    }

    public function includeTasks($project)
    {
        $userId = $this->auth->user()->id;
        $tasks = $project->tasks()
            ->incomplete()
            ->where(function ($q) use (&$userId) {
                $q->where('user_id', $userId)
                ->orWhere('user_id', null);
            })
            ->get();

        return $this->collection($tasks, new TaskTransformer);
    }
}
