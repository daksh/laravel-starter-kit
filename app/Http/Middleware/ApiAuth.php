<?php

namespace App\Http\Middleware;

use Closure;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        app('Dingo\Api\Auth\Auth')->extend('basic', function ($app) {
            return new Dingo\Api\Auth\Provider\Basic($app['auth'], 'email');
        });

        return $next($request);
    }
}
