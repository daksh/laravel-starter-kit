<?php

app('Dingo\Api\Transformer\Factory')->register(
    'App\Workspace',
    'App\Http\Controllers\Api\Transformers\WorkspaceTransformer'
);

app('Dingo\Api\Transformer\Factory')->register(
    'App\Project',
    'App\Http\Controllers\Api\Transformers\ProjectTransformer'
);

app('Dingo\Api\Transformer\Factory')->register(
    'App\Task',
    'App\Http\Controllers\Api\Transformers\TaskTransformer'
);
