<?php

namespace App\Console\Commands;

use App\Asana;
use Illuminate\Console\Command;

class SyncProjectsCommand extends Command
{
    protected $asana;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:projects';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync projects from Asana.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Asana $asana)
    {
        parent::__construct();

        $this->asana = $asana;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        if (!$this->asana->isAuthorized()) {
            $this->warn('Obtain your token from: '.$this->asana->getAuthUrl());

            $token = $this->ask('Enter your token: ');

            $this->asana->client->dispatcher->fetchToken($token);
        }

        $user = $this->asana->user();
    }
}
