<?php

namespace App\Scopes;

use App\Scopes\SelectWorkspaceScope;

trait SelectWorkspaceTrait
{
    public static function bootSelectWorkspaceTrait()
    {
        static::addGlobalScope(new SelectWorkspaceScope);
    }
}
