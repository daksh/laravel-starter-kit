//Flot Pie Chart
$(function() {

    var data = [{
        label: "Non-Billable",
        data: 1,
    }, {
        label: "Billable",
        data: 3,
        color: '#45b34a'
    }];

    var options = {
        series: {
            pie: {
                show: true,
                innerRadius: 0.5
            }
        },
        grid: {
            hoverable: true
        },
        legend: {
            show: true,
            labelFormatter: function(label, series) {
                // series is the series object for the label
                return '';
            }
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0 Hrs, %s", // show percentages, rounding to 2 decimal places
            defaultTheme: false
        }
    };

    var thisMonth = $.plot($("#this-month"), data, options);
    var lastMonth = $.plot($("#last-month"), data, options);

});
