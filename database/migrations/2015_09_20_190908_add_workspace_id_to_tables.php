<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWorkspaceIdToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            //
            $table->integer('workspace_id')->unsigned();

            $table->foreign('workspace_id')->references('id')->on('workspaces')->onDelete('cascade');
        });

        Schema::table('time_entries', function (Blueprint $table) {
            //
            $table->integer('workspace_id')->unsigned();

            $table->foreign('workspace_id')->references('id')->on('workspaces')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            //
            $table->dropForeign('tasks_workspace_id_foreign');

            $table->dropColumn('workspace_id');
        });

        Schema::table('time_entries', function (Blueprint $table) {
            //
            $table->dropForeign('time_entries_workspace_id_foreign');

            $table->dropColumn('workspace_id');
        });
    }
}
