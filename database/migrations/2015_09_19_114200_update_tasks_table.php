<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->string('asana_id')->unique();
            $table->integer('project_id')->unsigned();
            $table->string('title');
            $table->text('description')->nullable();
            $table->integer('user_id')->unsigned()->nullable();

            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            //
            $table->dropForeign('tasks_project_id_foreign');
            $table->dropForeign('tasks_user_id_foreign');
            $table->dropColumn(['asana_id', 'project_id', 'title', 'description', 'user_id']);
        });
    }
}
