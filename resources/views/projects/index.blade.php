@extends('layouts.default')

@section('title')
Projects
@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">All Projects</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-md-12">
        @if(count($projects) > 0)
        @foreach($projects as $project)
        <div class="col-lg-4">
            <div class="fixheight panel panel-{{ ($project->archived == false) ? 'info' : 'danger' }}" data-archived="{{ $project->archived }}">
                <div class="panel-heading">
                    {{ $project->name }}
                </div>
                <div class="panel-body">
                    <p><strong>Time worked:</strong> <br/> {{ $project->totalTime() }}</p>
                    <div class="row">
                        <p class="col-md-6"><strong>Unpaid:</strong> <br/> {{ $project->totalUnpaidTime() }}</p>
                        <p class="col-md-6"><strong>Tasks:</strong> <small><a href="{{ url('tasks/sync/'.$project->id) }}">Sync</a></small> <br/> {{ $project->noOfPendingTasks() }}/{{ $project->noOfTasks() }}</p>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <a href="{{ url('projects/'.$project->id) }}">Explore</a>
                </div>
            </div>
        </div>
        @endforeach
        @else
        <p>There are no projects. Try <a href="{{ url('projects/sync') }}">sync</a> with your asana account.</p>
        @endif
    </div>
</div>
@stop

@section('footer')

@stop
