@extends('layouts.auth')

{{-- Page title --}}
@section('title')
Account Sign in ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-body">
                <a href="{{ url('asana/login') }}" class="btn btn-lg btn-success btn-block">Login with Asana</a>
            </div>
        </div>
    </div>
</div>
@stop
