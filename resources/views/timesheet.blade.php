@extends('layouts.default')

@section('title')
Timesheet
@stop

@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('fullcalendar/fullcalendar.css') }}" />
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">Timesheet</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div id="calendar"></div>
    </div>
</div>
@stop

@section('footer')
<script type="text/javascript" src="{{ asset('fullcalendar/lib/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('fullcalendar/fullcalendar.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#calendar").fullCalendar({

        });
    });
</script>
@stop
